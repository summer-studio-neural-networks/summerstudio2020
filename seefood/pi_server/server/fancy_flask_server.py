import sys
import os
from flask import Flask, redirect, url_for, request, render_template, session
from werkzeug.utils import secure_filename
from aux import *

# Define a flask app and configure an upload directory
app = Flask(import_name = 'fancy flask', static_folder = "fancy_static", template_folder ="fancy_template")
app.config['UPLOAD_FOLDER'] = 'fancy_static/uploads/'

# Generate the HTML template that we have constructed
@app.route('/', methods=['GET'])
def home():
	return render_template('upload.html', result = resultTest, url = file_path)

# Generate the prediction from the uploads folder
@app.route('/', methods=['POST'])
def upload_file():
	if request.method == 'POST':
		if request.form['submit_button'] == 'Submit':
			if cam_used == False:
				f = request.files['file']
				# Save the file to ./uploads
				file_path = os.path.join(
				app.config['UPLOAD_FOLDER'], secure_filename(f.filename))
				f.save(file_path)
				abs_file_path = os.path.abspath(file_path)
				# Make prediction
				x = process_image(abs_file_path)
				output = model.get_output(x)
			else:
				resultTest = session.get('result')
				file_path = session.get('filepath')

			resultTest = get_class(output)

			return render_template('result.html', result = resultTest, url = file_path)

# //rendering the HTML page which has the button
@app.route('/json')
def json():
	return render_template('upload.html')

# //background process happening without any refreshing
@app.route('/background_process_test', methods=['POST', 'GET'])
def background_process_test():
	# code that handles camera option in background of web page
	cam_used = True
	file_path = "fancy_static/uploads/0.jpg"
	x = process_image(file_path)
	output = model.get_output(x)
	resultTest = get_class(output)
	session['result'] == resultTest
	session['filepath'] == file_path

	# returns the original html as this is a background operation
	return render_template('upload.html', results=resultTest, url=file_path)

if __name__ == '__main__':
	resultTest = str()
	file_path = str()
	cam_used = False
	model = Model()
	app.run(host='0.0.0.0', debug = False)
